package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        list.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        list.remove(entity);
    }

    @Override
    public int size() {
        return list.size();
    }

    public E removeById(@NotNull final String id) {
        list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    public E findById (@NotNull final String id){
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }
}



