package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public List<Task> findALLTaskByProjectId(@NotNull final String userId,@Nullable final String projectId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<Task> removeAllTaskByProjectId(@Nullable final String projectId,@NotNull final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .forEach(this::remove);
        return null;
    }

    @Nullable
    @Override
    public Task assignTaskByProjectId(@NotNull final String userId, @NotNull final String projectId,@NotNull  final String taskId) {
        @Nullable final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unassignTaskByProjectId(@NotNull final String userId,@NotNull  final String taskId) {
        @Nullable final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(@NotNull final String userId,@NotNull Task task) {
        List<Task> list = findAll(userId);
        list.add(task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        List<Task> list = findAll(userId);
        list.remove(task);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId,@Nullable final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId,@NotNull final Integer index) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @Nullable final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId,@Nullable final String name) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Nullable
    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @Nullable final String name) {
        return list.stream()
                .anyMatch(e -> name.equals(e.getName()));
    }

    @Override
    public String getIdByIndex(@NotNull int index) {
        return list.get(index).getId();
    }

}








