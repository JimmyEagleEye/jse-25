package ru.korkmasov.tsc.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.model.AbstractOwner;
import ru.korkmasov.tsc.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwner> extends IRepository<E> {

    void clear(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    int size(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);


}
