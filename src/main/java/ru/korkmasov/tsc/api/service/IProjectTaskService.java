package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.Task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.util.List;

public interface IProjectTaskService {

    @Nullable
    List<Task> findALLTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    @Nullable
    Task assignTaskByProjectId(final String userId, final String taskId, final @Nullable String projectId);

    @Nullable
    Task unassignTaskByProjectId(@NotNull String userId, @NotNull String taskId);

    @Nullable
    List<Task> removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Project removeProjectById(@NotNull String userId, @NotNull String projectId);

    void removeProjectByName(@NotNull String userId, @Nullable String projectName);

}
