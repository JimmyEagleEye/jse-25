package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

import org.jetbrains.annotations.NotNull;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

}
