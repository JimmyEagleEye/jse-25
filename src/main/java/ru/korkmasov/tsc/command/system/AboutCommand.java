package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

import org.jetbrains.annotations.NotNull;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public final String name() {
        return "about";
    }

    @NotNull
    @Override
    public final String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Djalal Korkmasov");
        System.out.println("dkorkmasov@t1-consulting.com");
    }
}
