package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectByNameFinishCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-finish-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Finish project by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        serviceLocator.getProjectService().finishProjectByName(userId, TerminalUtil.nextLine());
    }

}
