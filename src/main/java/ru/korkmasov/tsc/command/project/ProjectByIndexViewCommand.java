package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectByIndexViewCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-view-by-index";
    }

    @Override
    public @NotNull String description() {
        return "View project by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().findOneByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
