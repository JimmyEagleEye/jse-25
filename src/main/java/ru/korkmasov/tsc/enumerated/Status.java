package ru.korkmasov.tsc.enumerated;

import ru.korkmasov.tsc.exception.entity.StatusNotFoundException;
import ru.korkmasov.tsc.util.ValidationUtil;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static void main(String[] args) {
        System.out.println(COMPLETE);
    }

    public static Status getStatus(@Nullable String s) {
        if (s == null) throw new StatusNotFoundException();
        s = s.toUpperCase();
        if (!ValidationUtil.checkStatus(s)) throw new StatusNotFoundException();
        return Status.valueOf(s);
    }

}




