package ru.korkmasov.tsc.enumerated;

import ru.korkmasov.tsc.exception.entity.RoleNotFoundException;

import static ru.korkmasov.tsc.util.ValidationUtil.checkRole;

public enum Role {
    USER("User"),
    ADMIN("Admin");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public static Role getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

    public String getDisplayName() {
        return displayName;
    }
}
