package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.api.service.IPropertyService;
import ru.korkmasov.tsc.api.service.IUserService;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.exception.system.AccessDeniedException;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyLoginException;
import ru.korkmasov.tsc.exception.empty.EmptyPasswordException;
import ru.korkmasov.tsc.exception.user.NotLoggedInException;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.HashUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static ru.korkmasov.tsc.util.ValidationUtil.isEmpty;

public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        @Nullable final String userId = getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new EmptyIdException();
        return user;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        userId = null;
    }

    @Override
    public void login(@Nullable final String login,@Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash()) || user.isLocked()) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login,@Nullable final String password, final String email) {
        userService.add(login, password, email);
    }
}

